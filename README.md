# Flexible-Manipulator
Modelling of a flexible manipulator using Differential Flatness

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
To see calculations, refer to nlinkctrb.m
To see final controllability and linearized matrices, refer to linearized.m
To see animation, open sim_nlink.m and run the file.
    Vary the initial values for y0 = [theta1 theta2 theta3 w1 w2 w3]
    Vary timespan
    Vary input forces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

References:
http://www.cds.caltech.edu/~murray/books/AM08/pdf/obc-complete_15Feb10.pdf
http://www.cds.caltech.edu/~murray/preprints/mmr03-cds.pdf
